/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */
using namespace std;
#include <iostream>
#include <fstream>
#include "Circle.h"

Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	this->r = r; 
}

void Circle::setR(int r) {
	this->r = r;
}

double Circle::getR() const{
	return r;
}

double Circle::calculateCircumference() const{
	return PI * r * r;
}

double Circle::calculateArea(){
	return PI * r * 2;
}

bool Circle::equals(int a, int b) {
	if (a == b) {
		cout << "They are equal!" << endl;
		return true;
	}
	else {
		cout << "They are not equal!" << endl;
		return false;
	}
}
